package com.shpp;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.shpp.db.DataBaseEditor;
import com.shpp.dto.StocksPojo;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class DataBaseEditorTest {

    @Test
    void generateShops() {
        MongoCollection<Document> collection = Mockito.mock(MongoCollection.class);
        ((LoggerContext) LoggerFactory.getILoggerFactory())
                .getLogger("org").setLevel(Level.WARN);
        DataBaseEditor editor = new DataBaseEditor();
        editor.setShopCollection(collection);
        List<Document> list = editor.generateShops();

        assertEquals(7, list.size());
    }

    @Test
    void generateStocks() {
        MongoCollection<StocksPojo> collection = Mockito.mock(MongoCollection.class);
        ((LoggerContext) LoggerFactory.getILoggerFactory())
                .getLogger("org").setLevel(Level.WARN);
        DataBaseEditor editor = new DataBaseEditor();
        editor.setStockCollection(collection);
        List<StocksPojo> list = editor.generateStocks(100001);

        assertEquals(2, list.size());

        for (int i = 0; i < list.size(); i++) {
            StocksPojo stock = list.get(i);
            String name = stock.getProduct().getProductName();
            boolean isValid = name.length() >= 5 && name.length() <= 15;

            assertTrue(isValid);
        }
    }
}