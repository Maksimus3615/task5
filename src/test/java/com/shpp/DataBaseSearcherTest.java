package com.shpp;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.shpp.db.DataBaseSearcher;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class DataBaseSearcherTest {

    @Test
    void findShop() {
        MongoDatabase mongoDB = Mockito.mock(MongoDatabase.class);
        MongoCollection<Document> collection = Mockito.mock(MongoCollection.class);
        AggregateIterable aggregate = Mockito.mock(AggregateIterable.class);
        FindIterable findIterable = Mockito.mock(FindIterable.class);

        when(mongoDB.getCollection(anyString())).thenReturn(collection);
        when(collection.aggregate(anyList())).thenReturn(aggregate);
        when(aggregate.first()).thenReturn(new Document("_id", 1).append("total", 199));
        when(collection.find(any(Bson.class))).thenReturn(findIterable);
        when(findIterable.first()).thenReturn(new Document("address", "anyAddress"));

        DataBaseSearcher searcher = new DataBaseSearcher(mongoDB);
        String result = searcher.findShop("type");
        assertEquals("anyAddress", result);
    }
    @Test
    void notFindShop() {
        MongoDatabase mongoDB = Mockito.mock(MongoDatabase.class);
        MongoCollection<Document> collection = Mockito.mock(MongoCollection.class);
        AggregateIterable aggregate = Mockito.mock(AggregateIterable.class);
        FindIterable findIterable = Mockito.mock(FindIterable.class);

        when(mongoDB.getCollection(anyString())).thenReturn(collection);
        when(collection.aggregate(anyList())).thenReturn(aggregate);
        when(aggregate.first()).thenReturn(null);
        when(collection.find(any(Bson.class))).thenReturn(findIterable);
        when(findIterable.first()).thenReturn(null);

        DataBaseSearcher searcher = new DataBaseSearcher(mongoDB);
        String result = searcher.findShop("type");
        assertEquals("unable to find address...", result);
    }
}