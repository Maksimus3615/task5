package com.shpp.dto;

public class StocksPojo {
    private final int shopId;
    private final int amount;
    private final ProductPojo product;

    public StocksPojo(int shopId, int amount, ProductPojo product) {
        this.shopId = shopId;
        this.amount = amount;
        this.product = product;
    }

    public int getShopId() {
        return shopId;
    }

    public int getAmount() {
        return amount;
    }

    public ProductPojo getProduct() {
        return product;
    }
}
