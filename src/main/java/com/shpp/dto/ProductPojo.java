package com.shpp.dto;

import jakarta.validation.constraints.NotNull;

public class ProductPojo {
    @NotNull
    @CheckNameSize(message = "the name must contain at least 5 letters and no more than 15 letters")
    private final String productName;
    @NotNull
    private final String productType;
    public ProductPojo(String name, String type) {
        this.productName = name;
        this.productType = type;
    }

    public String getProductType() {
        return productType;
    }

    public String getProductName() {
        return productName;
    }
}
