package com.shpp.db;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Aggregates.limit;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;
import static java.util.Arrays.asList;

public class DataBaseSearcher {
    MongoCollection<Document> stockCollection;
    MongoCollection<Document> shopCollection;
    private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseSearcher.class);

    public DataBaseSearcher(MongoDatabase mongoDB) {
        this.stockCollection = mongoDB.getCollection("stocks");
        this.shopCollection = mongoDB.getCollection("shops");
    }

    public String findShop(String type) {
        StopWatch timer = new StopWatch();
        LOGGER.info("starts shop searching ...");
        timer.start("shops searching");

        int shopId = findShopId(type);
        String address;
        Document resultDoc = shopCollection.find(eq("_id", shopId)).first();
        if (shopId != 0 && resultDoc != null) {
            address = resultDoc.get("address").toString();
        } else address = "unable to find address...";

        timer.stop();
        String mess ="shops searching time was " + timer.getTotalTimeSeconds() + " sec.";
        LOGGER.info(mess);
        return address;
    }

    private int findShopId(String type) {
        int shopId;
        String fieldName = "total";     //sum of amounts
        Bson match1 = match(eq("product.productType", type));
        Bson group1 = group("$shopId", sum(fieldName, "$amount"));
        Bson sort1 = sort(descending(fieldName));
        Bson limit1 = limit(1);
        Document resultDoc = stockCollection.aggregate(asList(match1, group1, sort1, limit1)).first();
        if (resultDoc != null) {
            shopId = (int) resultDoc.get("_id");
            String mess = "the type you were looking for was found in shop number " +
                    shopId + ", the quantity is " + resultDoc.get(fieldName).toString();
            LOGGER.info(mess);
        } else {
            shopId = 0;
            LOGGER.info("no way to determine shopId...");
        }
        return shopId;
    }
}
