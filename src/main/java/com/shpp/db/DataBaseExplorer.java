package com.shpp.db;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.shpp.property.PropertyGetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class DataBaseExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseExplorer.class);
    private static final int NUMBER_OF_STOCKS = 500000;

    public static void main(String[] args) {

        Properties properties = PropertyGetter.getProperties("mongoDB.properties");
        setLoggerLevel();

        String uri = properties.getProperty("url");
        MongoClient client = new MongoClient(new MongoClientURI(uri));
        MongoDatabase mongoDB = client.getDatabase("EpicShop");
        DataBaseEditor editor = new DataBaseEditor(mongoDB);

        editor.dropCollections();
        editor.generateShops();
        editor.generateStocks(NUMBER_OF_STOCKS);

        DataBaseSearcher searcher = new DataBaseSearcher(mongoDB);
        String address = searcher.findShop(getProductId(args));

        LOGGER.info("**********************");
        LOGGER.info(address);
        LOGGER.info("**********************");

        client.close();
    }

    private static void setLoggerLevel() {
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org").setLevel(Level.WARN);
    }

    private static String getProductId(String[] args) {
        String type = "type1";
        if (args.length != 0) {
            type = args[0];
            String mess = "Type of product set to \"" + type + "\"";
            LOGGER.warn(mess);
        } else
            LOGGER.warn("Default type of product set to \"type1\"");
        return type;
    }
}