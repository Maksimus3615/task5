package com.shpp.db;

import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.shpp.dto.ProductPojo;
import com.shpp.dto.StocksPojo;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DataBaseEditor {
    MongoCollection<StocksPojo> stockCollection;
    MongoCollection<Document> shopCollection;

    public void setStockCollection(MongoCollection<StocksPojo> stockCollection) {
        this.stockCollection = stockCollection;
    }

    public void setShopCollection(MongoCollection<Document> shopCollection) {
        this.shopCollection = shopCollection;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseEditor.class);
    private static final Random random = new Random();
    private static final int LENGTH_NAME_MIN = 5;
    private static final int LENGTH_NAME_MAX = 15;
    private static final int NOT_LETTER_SYMBOL_MIN = 91;
    private static final int NOT_LETTER_SYMBOL_MAX = 96;
    private static final int NUMBER_OF_LETTER_MIN = 65;
    private static final int NUMBER_OF_LETTER_MAX = 123;
    private static final int NUMBER_OF_SHOPS = 7;
    private static final int NUMBER_OF_TYPES = 100;
    private static final int MAX_AMOUNT = 1000;
    private static final int BATCH = 100000;
    private static final String UNITS_OF_TIME = " sec.";

    public DataBaseEditor() {
    }

    public DataBaseEditor(MongoDatabase mongoDB) {
        this.stockCollection = mongoDB.withCodecRegistry(
                        CodecRegistries.fromRegistries(
                                MongoClientSettings.getDefaultCodecRegistry(),
                                CodecRegistries.fromProviders(PojoCodecProvider
                                        .builder()
                                        .automatic(true)
                                        .build())))
                .getCollection("stocks", StocksPojo.class);
        this.shopCollection = mongoDB.getCollection("shops");
    }

    public void dropCollections() {
        StopWatch timer = new StopWatch();
        LOGGER.info("starts deleting collections from the database...");
        timer.start("dropping");

        stockCollection.drop();
        shopCollection.drop();

        timer.stop();
        String mess = "dropping time was " + timer.getTotalTimeSeconds() + UNITS_OF_TIME;
        LOGGER.info(mess);
    }

    public List<Document> generateShops() {
        StopWatch timer = new StopWatch();
        LOGGER.info("starts shops generating ...");
        timer.start("shops generating");

        String[] address = {"Kyiv, Sobornaya 122",
                "Lviv, Konovalcya 154",
                "Kropyvnytskyi, Popova 8",
                "Kramatorsk, M.Pryimachenko 14",
                "Sumy, Drujby 56'",
                "Odesa, Paustovskogo 14",
                "Dnipro, Svobody 26"};
        List<Document> list = new ArrayList<>();
        int iterator = 1;
        for (String a : address) {
            Document doc = new Document("_id", iterator);
            doc.append("address", a);
            list.add(doc);
            iterator++;
        }
        shopCollection.insertMany(list);

        timer.stop();
        String mess = "shops generating time was " + timer.getTotalTimeSeconds() + UNITS_OF_TIME;
        LOGGER.info(mess);
        return list;
    }

    public List<StocksPojo> generateStocks(int limit) {
        StopWatch timer = new StopWatch();
        LOGGER.info("starts stocks generating ...");
        timer.start("stocks generating");

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        List<StocksPojo> list = new ArrayList<>();
        List<StocksPojo> testList = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger(1);
        Stream.generate(() -> new StocksPojo(getRandomId(), getRandomAmount(),
                        new ProductPojo(getRandomName(getRandomLength()), getRandomType())))
                .filter(x -> validator.validate(x.getProduct()).isEmpty()).limit(limit).
                forEach(i -> {
                    list.add(i);
                    if (counter.get() == limit || counter.get() % BATCH == 0) {
                        stockCollection.insertMany(list);
                        String mess = counter.get() + " documents successfully sent";
                        LOGGER.info(mess);
                        list.clear();
                        testList.add(i);
                    }
                    counter.getAndIncrement();
                });
        validatorFactory.close();

        timer.stop();
        String mess = "stocks generating time was " + timer.getTotalTimeSeconds() + UNITS_OF_TIME;
        LOGGER.info(mess);
        LOGGER.info("-----------------------------");
        mess = "RPS is " + limit / timer.getTotalTimeSeconds();
        LOGGER.info(mess);
        LOGGER.info("-----------------------------");
        return testList;
    }

    private int getRandomAmount() {
        return random.nextInt(MAX_AMOUNT); // 0...999
    }

    private int getRandomId() {
        return random.nextInt(NUMBER_OF_SHOPS) + 1; // 1...7
    }

    private String getRandomType() {
        return "type" + (random.nextInt(NUMBER_OF_TYPES) + 1); // 1...100
    }

    private int getRandomLength() {
        return random.nextInt(LENGTH_NAME_MAX - LENGTH_NAME_MIN) + LENGTH_NAME_MIN;
    }

    private String getRandomName(int length) {
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int charNum = random.nextInt(NUMBER_OF_LETTER_MAX - NUMBER_OF_LETTER_MIN) + NUMBER_OF_LETTER_MIN;
            if (charNum < NOT_LETTER_SYMBOL_MIN || charNum > NOT_LETTER_SYMBOL_MAX) {
                name.append((char) (charNum));
            }
        }
        return name.toString();
    }
}
